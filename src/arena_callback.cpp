#include "arena_camera/arena_callback.h"

#include <opencv2/opencv.hpp>

#include <utility>
#include <thread>

ArenaCallback::ArenaCallback(
  std::uint32_t camera_idx, std::string & frame_id, uint32_t width, uint32_t height)
: m_camera_idx(camera_idx),
  m_frame_id(frame_id),
  m_width(width),
  m_height(height)
{}

void ArenaCallback::OnImage(Arena::IImage * pImage)
{
  try {
    m_signal_publish_image(m_camera_idx, convert_to_image_msg(pImage, m_frame_id));
  } catch (std::exception & e) {
    std::cerr << "Exception m_signal_publish_image : " << e.what() << std::endl;
  }
}

std::unique_ptr<sensor_msgs::msg::Image> ArenaCallback::convert_to_image_msg(
  Arena::IImage * pImage, const std::string & frame_id)
{

  auto image = Arena::ImageFactory::Convert(pImage, BGR8);
  auto msg{std::make_unique<sensor_msgs::msg::Image>()};

  const size_t image_size = image->GetWidth() * 3.0;

  cv::Mat image_cv = cv::Mat(
    image->GetHeight(),
    image->GetWidth(),
    CV_8UC3,
    (uint8_t *) image->GetData(),
    image_size);

  cv::resize(image_cv, image_cv, cv::Size(m_width, m_height));

  msg->height = static_cast<std::uint32_t>(image_cv.rows);
  msg->width = static_cast<std::uint32_t>(image_cv.cols);
  msg->is_bigendian = 1;
  msg->encoding = "bgr8";
  msg->header.frame_id = frame_id;
  const size_t image_size_resized = image_cv.rows * image_cv.cols * 3.0;
  msg->data.resize(static_cast<std::uint32_t>(image_size_resized));
  std::copy_n(image_cv.data, image_size_resized, msg->data.data());

  Arena::ImageFactory::Destroy(image);

  return msg;
}

void ArenaCallback::set_on_image_callback(ImageCallbackFunction callback)
{
  m_signal_publish_image = std::move(callback);
}
