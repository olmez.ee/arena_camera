#include "arena_camera/arena_camera_node.h"
#include "arena_camera/camera_settings.h"

#include <rcutils/logging_macros.h>
#include <rclcpp_components/register_node_macro.hpp>

ArenaCameraNode::ArenaCameraNode(const rclcpp::NodeOptions & node_options)
: rclcpp::Node{
    "arena_camera_node", node_options}
{
  m_arena_camera_handler = std::make_unique<ArenaCamerasHandler>();
  auto camera_settings = read_camera_settings();
  m_arena_camera_handler->create_cameras_from_settings(camera_settings);
  m_publishers = create_publishers(
    this,
    m_arena_camera_handler->get_camera_count());

  m_arena_camera_handler->set_image_callback(
    std::bind(
      &ArenaCameraNode::publish_image, this,
      std::placeholders::_1,
      std::placeholders::_2));

  m_arena_camera_handler->start_stream();
}

std::vector<CameraSetting> ArenaCameraNode::read_camera_settings()
{
  std::vector<CameraSetting> camera_setting;
  const std::string cameras_param_name{"camera_names"};
  const auto camera_names{declare_parameter(cameras_param_name, std::vector<std::string>{})};
  RCLCPP_INFO(get_logger(), "Cameras size : %d", camera_names.size());
  for (std::size_t i = 0; i < camera_names.size(); i++) {
    std::string prefix = camera_names.at(i) + ".";
    camera_setting.emplace_back(
      camera_names.at(i),
      declare_parameter(prefix + "frame_id").template get<std::string>(),
      declare_parameter(prefix + "pixel_format").template get<std::string>(),
      declare_parameter(prefix + "serial_no").template get<uint32_t>(),
      declare_parameter(prefix + "fps").template get<uint32_t>(),
      declare_parameter(prefix + "width").template get<uint32_t>(),
      declare_parameter(prefix + "height").template get<uint32_t>()
    );
  }
  return camera_setting;
}

std::vector<ArenaCameraNode::ProtectedPublisher>
ArenaCameraNode::create_publishers(
  ::rclcpp::Node * node,
  const size_t number_of_cameras)
{
  if (!node) {
    throw std::runtime_error("The node is not initialized. Cannot create publishers.");
  }

  std::vector<ProtectedPublisher> publishers(number_of_cameras);
  for (auto i = 0U; i < number_of_cameras; ++i) {
    publishers.at(i).m_publisher =
      image_transport::create_camera_publisher(node, create_camera_topic_name(i));
  }
  return publishers;
}

void ArenaCameraNode::publish_image(
  std::uint32_t camera_index,
  std::unique_ptr<sensor_msgs::msg::Image> image)
{

  if (m_publishers.at(camera_index).m_publisher) {
    if (image) {
      const std::lock_guard<std::mutex> lock{m_publishers.at(camera_index).m_publish_mutex};
      image->header.stamp = this->now();
      auto camera_info = std::make_unique<sensor_msgs::msg::CameraInfo>();
      m_publishers.at(camera_index).m_publisher.publish(*image, *camera_info);
    }
  } else {
    throw std::runtime_error("Publisher is nullptr, cannot publish.");
  }
}

RCLCPP_COMPONENTS_REGISTER_NODE(ArenaCameraNode)
