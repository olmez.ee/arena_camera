#include "arena_camera/arena_camera.h"

ArenaCamera::ArenaCamera(
  Arena::IDevice * device,
  CameraSetting & camera_setting,
  uint32_t camera_idx)
: m_device(device),
  m_camera_name(camera_setting.get_camera_name()),
  m_frame_id(camera_setting.get_frame_id()),
  m_pixel_format(camera_setting.get_pixel_format()),
  m_serial_no(camera_setting.get_serial_no()),
  m_fps(camera_setting.get_fps()),
  m_cam_idx(camera_idx),
  m_width(camera_setting.get_width()),
  m_height(camera_setting.get_height()){

  m_image_callback = new ArenaCallback(m_cam_idx, m_frame_id, m_width, m_height);

  Arena::SetNodeValue<bool>(m_device->GetTLStreamNodeMap(), "StreamAutoNegotiatePacketSize", true);
  Arena::SetNodeValue<bool>(m_device->GetTLStreamNodeMap(), "StreamPacketResendEnable", true);

  auto node_map = m_device->GetNodeMap();
  auto max_fps = GenApi::CFloatPtr(node_map->GetNode("AcquisitionFrameRate"))->GetMax();
  Arena::SetNodeValue<GenICam::gcstring>(node_map, "GainAuto", "Continuous");
  if (m_fps > max_fps) {
    std::cout << "max" << std::endl;
    Arena::SetNodeValue<bool>(node_map, "AcquisitionFrameRateEnable", true);
    Arena::SetNodeValue<double>(node_map, "AcquisitionFrameRate", max_fps);
  } else {
    Arena::SetNodeValue<bool>(node_map, "AcquisitionFrameRateEnable", true);
    Arena::SetNodeValue<double>(node_map, "AcquisitionFrameRate", static_cast<double>(m_fps));
  }

}

ArenaCamera::ArenaCamera(
  Arena::IDevice * device,
  std::string & camera_name,
  std::string & frame_id,
  std::string & pixel_format,
  uint32_t serial_no,
  uint32_t fps,
  uint32_t camera_idx,
  uint32_t width,
  uint32_t height)
: m_device(device),
  m_camera_name(camera_name),
  m_frame_id(frame_id),
  m_pixel_format(pixel_format),
  m_serial_no(serial_no),
  m_fps(fps),
  m_cam_idx(camera_idx),
  m_width(width),
  m_height(height){

  m_image_callback = new ArenaCallback(m_cam_idx, m_frame_id, m_width, m_height);

  Arena::SetNodeValue<bool>(m_device->GetTLStreamNodeMap(), "StreamAutoNegotiatePacketSize", true);
  Arena::SetNodeValue<bool>(m_device->GetTLStreamNodeMap(), "StreamPacketResendEnable", true);

  auto node_map = m_device->GetNodeMap();
  auto max_fps = GenApi::CFloatPtr(node_map->GetNode("AcquisitionFrameRate"))->GetMax();
  Arena::SetNodeValue<GenICam::gcstring>(node_map, "GainAuto", "Continuous");
  if (m_fps > max_fps) {
    std::cout << "max" << std::endl;
    Arena::SetNodeValue<bool>(node_map, "AcquisitionFrameRateEnable", true);
    Arena::SetNodeValue<double>(node_map, "AcquisitionFrameRate", max_fps);
  } else {
    Arena::SetNodeValue<bool>(node_map, "AcquisitionFrameRateEnable", true);
    Arena::SetNodeValue<double>(node_map, "AcquisitionFrameRate", static_cast<double>(m_fps));
  }
}

void ArenaCamera::start_stream()
{
  future_ = std::async(std::launch::async, &ArenaCamera::acquisition, this);
}

void ArenaCamera::acquisition(){
  m_device->RegisterImageCallback(m_image_callback);
  m_device->StartStream();
}

void ArenaCamera::stop_stream()
{
  m_device->StopStream();
}

void ArenaCamera::destroy_device(Arena::ISystem * system)
{
  m_device->DeregisterImageCallback(m_image_callback);
  if (m_device != nullptr) {
    system->DestroyDevice(m_device);
  }
}

ArenaCamera::~ArenaCamera()
{
  if (m_device) {
    m_device->StopStream();
  } else {
    std::cout << "No m_device " << std::endl;
  }

}
