#include <algorithm>
#include "arena_camera/arena_cameras_handler.h"

ArenaCamerasHandler::ArenaCamerasHandler()
{
  m_p_system = Arena::OpenSystem();
  m_p_system->UpdateDevices(100);
}

void ArenaCamerasHandler::create_cameras_from_settings(
  std::vector<CameraSetting> & camera_settings)
{
  std::vector<Arena::DeviceInfo> devicesInfos = m_p_system->GetDevices();

  if (devicesInfos.size() == 0) {
    throw std::runtime_error("arena_camera: There is no connected devices.");
  }

  if (devicesInfos.size() < camera_settings.size()) {
    throw std::runtime_error(
            "arena camera: Size of connected devices and cameras in params mismatch.");
  }

  m_cameras.reserve(camera_settings.size());
  
  uint32_t camera_idx = 0;
  for (std::size_t i = 0; i < camera_settings.size(); ++i) {

    auto it = std::find_if(
            devicesInfos.begin(), devicesInfos.end(),
      [&](Arena::DeviceInfo & d_info) {
        return std::to_string(camera_settings.at(i).get_serial_no()) == d_info.SerialNumber().c_str();
      });

    if (it != devicesInfos.end()) {
      Arena::IDevice * device = m_p_system->CreateDevice(*it);
      m_cameras.emplace_back(device, camera_settings.at(i), camera_idx);
      camera_idx++;
    } else {
      throw std::runtime_error("arena_camera: Wrong device serial no in parameters file.");
    }
  }
}

void ArenaCamerasHandler::set_image_callback(ArenaCallback::ImageCallbackFunction callback)
{
  for (auto & camera : m_cameras) {
    camera.m_image_callback->set_on_image_callback(callback);
  }
}

void ArenaCamerasHandler::start_stream()
{
  for (auto & camera : m_cameras) {
    camera.start_stream();
  }
}

void ArenaCamerasHandler::stop_stream()
{
  for (auto & camera : m_cameras) {
    camera.stop_stream();
  }
}

ArenaCamerasHandler::~ArenaCamerasHandler()
{
  CloseSystem(m_p_system);
  m_cameras.clear();
}
