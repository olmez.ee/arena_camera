#ifndef BUILD_CAMERA_SETTINGS_H
#define BUILD_CAMERA_SETTINGS_H

#include <string>
#include <iostream>

class CameraSetting {
public:
  explicit CameraSetting(
    const std::string & camera_name,
    const std::string & frame_id,
    const std::string & pixel_format,
    uint32_t serial_no,
    uint32_t fps,
    uint32_t width,
    uint32_t height)
  : m_camera_name {camera_name},
  m_frame_id {frame_id},
  m_pixel_format {pixel_format},
  m_serial_no {serial_no},
  m_fps {fps},
  m_width{width},
  m_height{height}
  {
    std::cout << "Camera readed from yaml file. Camera Name:" << m_camera_name <<
      " Frame id:" << m_frame_id <<
      " Serial no:" << m_serial_no <<
      " Pixel_format:" << m_pixel_format <<
      " FPS:" << m_fps <<
      std::endl;
  }


  std::string get_camera_name() {return m_camera_name;}
  std::string get_frame_id() {return m_frame_id;}
  std::string get_pixel_format() {return m_pixel_format;}
  uint32_t get_serial_no() {return m_serial_no;}
  uint32_t get_fps() {return m_fps;}
  uint32_t get_width() {return m_width;}
  uint32_t get_height() {return m_height;}

private:
  std::string m_camera_name;
  std::string m_frame_id;
  std::string m_pixel_format;
  uint32_t m_serial_no;
  uint32_t m_fps;
  uint32_t m_width;
  uint32_t m_height;
};


#endif //BUILD_CAMERA_SETTINGS_H
