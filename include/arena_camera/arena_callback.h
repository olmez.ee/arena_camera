#ifndef BUILD_ARENA_CALLBACK_H
#define BUILD_ARENA_CALLBACK_H

#include "Arena/ArenaApi.h"
#include <sensor_msgs/msg/image.hpp>

#include <string>
#include <memory>

class ArenaCallback: public Arena::IImageCallback {
public:
  explicit ArenaCallback(std::uint32_t camera_idx, std::string & frame_id, uint32_t width, uint32_t height);

  using ImageCallbackFunction = std::function < void(
    std::uint32_t,
    std::unique_ptr < sensor_msgs::msg::Image >) >;

  virtual void OnImage(Arena::IImage * pImage);

  ImageCallbackFunction m_signal_publish_image {};

  void set_on_image_callback(ImageCallbackFunction callback);

private:
  uint32_t m_camera_idx;

  std::string m_frame_id;

  uint32_t m_width;

  uint32_t m_height;

  std::unique_ptr < sensor_msgs::msg::Image > convert_to_image_msg(
    Arena::IImage * pImage, const std::string & frame_id);
};

#endif //BUILD_ARENA_CALLBACK_H
