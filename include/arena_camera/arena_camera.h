#ifndef BUILD_ARENA_CAMERA_H
#define BUILD_ARENA_CAMERA_H

#include "arena_camera/camera_settings.h"
#include "arena_camera/arena_callback.h"
#include "Arena/ArenaApi.h"

#include <string>

#include <iostream>
#include <future>

class ArenaCamera {
public:
  explicit ArenaCamera(
    Arena::IDevice * device,
    CameraSetting & camera_setting,
    uint32_t camera_idx);

  ArenaCamera(
    Arena::IDevice * device,
    std::string & camera_name,
    std::string & frame_id,
    std::string & pixel_format,
    uint32_t serial_no,
    uint32_t fps,
    uint32_t camera_idx,
    uint32_t width,
    uint32_t height);

  ~ArenaCamera();

  void start_stream();

  void stop_stream();

  void destroy_device(Arena::ISystem* system);

  void acquisition();

  ArenaCallback * m_image_callback;

private:
  Arena::IDevice * m_device;

  std::string m_camera_name;

  std::string m_frame_id;

  std::string m_pixel_format;

  uint32_t m_serial_no;

  uint32_t m_fps;

  uint32_t m_cam_idx;

  uint32_t m_width;

  uint32_t m_height;

  std::shared_future<void> future_;
};


#endif //BUILD_ARENA_CAMERA_H
