#ifndef BUILD_CAMERAS_WRAPPER_H
#define BUILD_CAMERAS_WRAPPER_H

#include "arena_camera/camera_settings.h"
#include "arena_camera/arena_camera.h"
#include "arena_camera/arena_callback.h"
#include "Arena/ArenaApi.h"

#include <vector>

class ArenaCamerasHandler {
public:
  explicit ArenaCamerasHandler();

  ~ArenaCamerasHandler();

  void init_arena();

  void create_cameras_from_settings(std::vector < CameraSetting > & camera_settings);

  void set_image_callback(ArenaCallback::ImageCallbackFunction callback);

  void start_stream();

  void stop_stream();

  int get_camera_count()
  {
    return m_cameras.size();
  }

private:
  std::vector < ArenaCamera > m_cameras;

  Arena::ISystem * m_p_system;

};


#endif //BUILD_CAMERAS_WRAPPER_H
