**arena_camera**

arena_camera node publishes image data collected from Lucid Vision Labs Triton GigE cameras in /lucid_vision/camera_"X"/image_raw topic as a ROS message(sensor_msgs/Image). This node can connect to multiple camera devices discovered by Lucid Vision Labs' ArenaSDK. In order to use this node, you need to install Lucid Vision Labs' ArenaSDK.

### Installation

1. Download ArenaSDK from [here](https://thinklucid.com/downloads-hub/).
2. Install ArenaSDK.
3. Before connecting to the camera you need to set your IP address, according to camera IP address.
4. Create a workspace for the driver and copy this driver to <your_workspace>/src/.
5. Build your code with following command.

`colcon build --cmake-args -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=1
`

6. Source the directory and run the executable with following command.

`ros2 run arena_camera arena_camera_node_exe --ros-args --params-file <your_workspace>/src/arena_camera/param/multi_camera.param.yaml `
